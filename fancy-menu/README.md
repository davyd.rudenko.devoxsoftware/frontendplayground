## Fancy Menu (JS+CSS+HTML)!
Inspired by [this video](https://www.youtube.com/watch?v=ArTVfdHOB-M&t=336s). <br> Also my first project created with Github Copilot :-) <br>
(**Spoiler:** I wasn't too impressed :-(. It was able to create some basic stuff though, like CSS for `.indicator::before` based on `.indicator::after` and some js based on comments).
<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="NWBdRqL" data-user="davydrudenko" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/davydrudenko/pen/NWBdRqL">
  Fancy Menu</a> by Davyd Rudenko (<a href="https://codepen.io/davydrudenko">@davydrudenko</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>