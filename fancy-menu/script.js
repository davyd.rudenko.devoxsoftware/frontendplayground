// for all .list elements add click event
document.querySelectorAll(".list").forEach(function (el) {
  el.addEventListener("click", function () {
    // remove all .active classes, then add .active to clicked element
    document.querySelectorAll(".list.active").forEach(function (el) {
      el.classList.remove("active");
    });
    this.classList.add("active");

    setIndicatorPosition();
  });
});

function setIndicatorPosition() {
  active = document.querySelector(".list.active");
  // set offsetLeft of .indicator to this.offsetLeft + this.offsetWidth / 2
  document.querySelector(".indicator").style.left =
    active.offsetLeft + active.offsetWidth / 2 + "px";
}

setIndicatorPosition();
