import { Injectable } from '@angular/core';
import { ITodo } from '../shared/models/todo.model';
import { Observable, delay, from, of, switchMap, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  todos: ITodo[] = [{
    id: crypto.randomUUID(),
    done: true,
    title: "Water the plants",
    description: "List on the fridge"
  },
  {
    id: crypto.randomUUID(),
    done: true,
    title: "Study NGXS",
    description: "https://www.ngxs.io/"
  },
  {
    id: crypto.randomUUID(),
    done: false,
    title: "Do the disches",
  },
  {
    id: crypto.randomUUID(),
    done: false,
    title: "Get the groceries",
    description: "Your favourite supermarket"
  }]

  constructor() { }

  getTodos(): Observable<ITodo[]> {
    return of(this.todos);
  }

  addTodo(options: { title: string, description: string }): Observable<ITodo> {
    return of({
      ...options,
      id: crypto.randomUUID(),
      done: false
    }).pipe(
      tap(todo => this.todos.push(todo))
    );
  }

  changeStatus(options: { id: string, done: boolean }): Observable<ITodo> {
    const index = this.todos.findIndex(todo => todo.id == options.id);
    if (index > -1) {
      this.todos[index].done = options.done;
      return of(this.todos[index]);
    }
    return of(undefined);
  }
}
