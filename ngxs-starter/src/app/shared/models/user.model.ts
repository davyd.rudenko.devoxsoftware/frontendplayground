export interface IUser {
    username: string;
    pictureUrl: string;
}