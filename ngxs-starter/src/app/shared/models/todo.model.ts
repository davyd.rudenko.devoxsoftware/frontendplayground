export interface ITodo {
    id: string;
    done: boolean;
    title: string;
    description?: string;
}