import { ITodo } from "src/app/shared/models/todo.model";

export interface TodosStateModel {
    todos: ITodo[];
}

export class GetTodos {
    static readonly type = "[TODOS] Get Todos";
}

export class AddTodo {
    static readonly type = "[TODOS] Add Todo";
    constructor(public payload: {
        title: string,
        description: string
    }) { }
}

export class ChangeStatus {
    static readonly type = "[TODOS] Change Todo Status";
    constructor(public payload: {
        id: string,
        done: boolean
    }) { }
}