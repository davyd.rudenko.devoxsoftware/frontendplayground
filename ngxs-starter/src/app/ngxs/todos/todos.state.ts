import { Injectable } from "@angular/core";
import { State, Action, Selector, StateContext, createSelector } from "@ngxs/store";
import { switchMap, tap } from "rxjs/operators";
import { AddTodo, ChangeStatus, GetTodos, TodosStateModel } from "./todos.actions";
import { TodosService } from "src/app/services/todos.service";
import { ITodo } from "src/app/shared/models/todo.model";

@State<TodosStateModel>({
    name: 'todos',
    defaults: {
        todos: null
    },
})
@Injectable()
export class TodosState {

    constructor(public todosService: TodosService) { }

    @Selector()
    static todos(state: TodosStateModel): ITodo[] {
        return state.todos;
    }

    static todosInState(done: boolean) {
        return createSelector([TodosState], (state: TodosStateModel) => state.todos.filter(todo => todo.done == done));
    }

    @Action(GetTodos)
    getTodos({ setState }: StateContext<TodosStateModel>) {
        return this.todosService.getTodos().pipe(
            tap((todos) => setState({
                todos
            }))
        );
    }

    @Action(AddTodo)
    addTodo({ getState, setState }: StateContext<TodosStateModel>, { payload }: AddTodo) {
        return this.todosService.addTodo(payload).pipe(
            tap(todo => {
                const state = getState();
                setState({
                    ...state,
                    todos: [
                        ...state.todos,
                        todo
                    ]
                })
            })
        )
    }

    @Action(ChangeStatus)
    changeStatus({ dispatch }: StateContext<TodosStateModel>, { payload }: ChangeStatus) {
        return this.todosService.changeStatus(payload).pipe(
            switchMap(todo => {
                return dispatch(GetTodos)
            })
        );
    }
}
