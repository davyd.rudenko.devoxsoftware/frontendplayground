import { IUser } from "src/app/shared/models/user.model";

export interface AuthStateModel {
    user: IUser;
}

export class Login {
    static readonly type = "[AUTH] Login";

    constructor(public payload: {
        username: string,
        password: string
    }) { }
}

export class Logout {
    static readonly type = "[AUTH] Logout";

    constructor() { }
}