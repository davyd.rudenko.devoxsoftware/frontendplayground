import { Injectable } from "@angular/core";
import { State, Action, Selector, StateContext } from "@ngxs/store";
import { AuthStateModel, Login, Logout } from "./auth.actions";
import { IUser } from "src/app/shared/models/user.model";
import { AuthService } from "src/app/services/auth.service";
import { tap } from "rxjs/operators";
import { Router } from "@angular/router";

@State<AuthStateModel>({
    name: 'auth',
    defaults: {
        user: null
    },
})
@Injectable()
export class AuthState {

    constructor(private authService: AuthService, private router: Router) { }

    @Selector()
    static user(state: AuthStateModel): IUser {
        return state.user;
    }

    @Selector()
    static isAuthorized(state: AuthStateModel): boolean {
        return !!state.user;
    }

    @Action(Login)
    login({ getState, setState }: StateContext<AuthStateModel>, { payload }: Login) {
        return this.authService.login(payload.username, payload.password).pipe(
            tap((user) => {
                const state = getState();
                setState({
                    ...state,
                    user
                });
                if(!!user) {
                    this.router.navigate(["todos"]);
                }
            })
        )
    }

    @Action(Logout)
    logout({ setState }: StateContext<AuthStateModel>) {
        setState({
            user: null
        });
        this.router.navigate(['auth', 'login']);
    }
}
