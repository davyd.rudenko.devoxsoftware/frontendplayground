import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';

const routes: Routes = [{
  path: '',
  component: MainLayoutComponent, children: [
    {
      path: '', pathMatch: 'full',
      redirectTo: 'todos'
    },
    {
      path: 'todos',
      canActivate: [AuthGuard],
      loadChildren: () => import('../../pages/todos/todos.module').then(m => m.TodosModule)
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainLayoutRoutingModule { }
