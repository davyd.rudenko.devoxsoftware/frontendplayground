import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Logout } from 'src/app/ngxs/auth/auth.actions';
import { AuthState } from 'src/app/ngxs/auth/auth.state';
import { IUser } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  @Select(AuthState.user) user$: Observable<IUser>;

  constructor(private store: Store) { }

  ngOnInit(): void {
  }

  onLogoutClick() {
    this.store.dispatch(new Logout());
  }

}
