import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ChangeStatus } from 'src/app/ngxs/todos/todos.actions';
import { TodosState } from 'src/app/ngxs/todos/todos.state';
import { ITodo } from 'src/app/shared/models/todo.model';

@Component({
  selector: 'app-todos-list',
  templateUrl: './todos-list.component.html',
  styleUrls: ['./todos-list.component.scss']
})
export class TodosListComponent {
  @Select(TodosState.todosInState(true))
  completedTodos$: Observable<ITodo[]>;

  @Select(TodosState.todosInState(false))
  unfinishedTodos$: Observable<ITodo[]>;

  constructor(private store: Store) {}

  onChangeTodoStatus(options: {id: string, done: boolean}) {
    this.store.dispatch(new ChangeStatus(options));
  }
}
