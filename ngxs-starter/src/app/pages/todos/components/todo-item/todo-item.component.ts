import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ITodo } from 'src/app/shared/models/todo.model';

@UntilDestroy()
@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit {
  @Input() todo: ITodo;

  @Output() todoStatusChange = new EventEmitter<{ id: string, done: boolean }>();

  doneCheckbox: FormControl;

  constructor() {}

  ngOnInit(): void {
    this.doneCheckbox = new FormControl(this.todo.done);

    this.doneCheckbox.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe((value: boolean) => this.todoStatusChange.next({ id: this.todo.id, done: value }));
  }

}
