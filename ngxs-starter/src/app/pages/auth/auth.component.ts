import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { Login } from 'src/app/ngxs/auth/auth.actions';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  loginForm: FormGroup;

  get usernameControl(): AbstractControl {
    return this.loginForm.controls['username'];
  }

  get passwordControl(): AbstractControl {
    return this.loginForm.controls['password'];
  }

  constructor(private formBuilder: FormBuilder, private store: Store) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group(
      {
        username: ['', [Validators.required]],
        password: ['', Validators.required]
      }
    );
  }

  onLoginFormSubmit() {
    const credentials = this.loginForm.value;
    this.store.dispatch(new Login(credentials));
  }

}
