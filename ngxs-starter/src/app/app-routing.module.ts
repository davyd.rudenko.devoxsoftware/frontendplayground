import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { TokenCheckGuard } from './guards/token-check.guard';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      canActivate: [AuthGuard],
      loadChildren: () => import('./modules/main-layout/main-layout.module').then(m => m.MainLayoutModule)
    },
    {
      path: 'auth',
      canActivate: [TokenCheckGuard],
      loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule)
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
